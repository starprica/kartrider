# KartRider Client

This repository is a dump of KartRider.exe and several guideline of legit features which may be useful to player.

## Available Features
Currently, the following features are available:
- Dumped client with loaded modules
- Custom resolution mode by hooking D3D (private)

## Changelog
- 2020-04-08 KR Server client.

More information is given in each file.